from cgitb import html
from flask import Flask, jsonify, render_template 


app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')
@app.route('/contact')
def contact():
    return render_template('contact.html')

app.run(port=5000, debug=True)
