
import csv
import pandas as pd
import re
from datetime import datetime
from ip_locator import get_location_info_from_ip, transform_to_dict, get_country


def to_datetime(string): return datetime.strptime(
    string, '%d/%b/%Y:%H:%M:%S %z')


def extract_timestamps(logs) -> list: return [to_datetime(
    re.search(r'\[(.*?)\]', log).group(1)) for log in logs]


def extract_ip_addresses(logs) -> list: return [log.split(
    ' - ')[0].split('] ')[1].split(' ')[0] for log in logs]


def extract_countries(ips) -> list: return [get_country(
    transform_to_dict(get_location_info_from_ip(ip))) for ip in ips]


def extract_user_agents(
    logs) -> list: return [log.split('""')[5] for log in logs]


def extract_request_urls(
    logs) -> list: return [log.split('""')[3] for log in logs]


def extract_response_codes(
    logs) -> list: return [re.sub(r'[a-zA-Z]', '', log.split('""')[2].split(' - ')[0]).strip().split(' ')[0] for log in logs]


def write_full_logs(logs:list) -> str:
    current_timestamp = datetime.now().strftime('%Y_%m_%d__%H_%M_%S')
    file_path = f"{current_timestamp}__logs_parse_ouput.log"
    with open(file_path, 'w') as file:
        writer = csv.writer(file)
        for log in logs:
            writer.writerow([log])
    return f"File written at {file_path}"


def read_full_logs_file(file_path) -> list:
    with open(file_path, 'r') as file:
        txt = file.read()
        logs = txt.split('\n')[0:-1]
    return logs


if __name__ == "__main__":

    logs = read_full_logs_file('2023_03_13__11_21_37__full_logs.log')
    timestamps = extract_timestamps(logs)
    ips = extract_ip_addresses(logs)
    countries = extract_countries(ips)
    user_agents = extract_user_agents(logs)
    request_urls = extract_request_urls(logs)
    response_codes = extract_response_codes(logs)

    df = pd.concat([pd.DataFrame(timestamps), pd.DataFrame(ips), pd.DataFrame(countries), pd.DataFrame(request_urls),
                    pd.DataFrame(user_agents), pd.DataFrame(response_codes)], True)

    df.columns = ["Request At", "Source IP address", "Country",
                  "Request URL", "User agent", "Response code"]
    
    current_timestamp = datetime.now().strftime('%Y_%m_%d__%H_%M_%S')

    df.to_csv(f'{current_timestamp}__logs_parse_output')
