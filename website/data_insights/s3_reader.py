import boto3

s3 = boto3.client('s3')
deafult_target_bucket = "logs-clauro-website"

# Get the list of objects in the bucket
objects = s3.list_objects(Bucket=deafult_target_bucket)


def read_logs_from_s3():
    lines = []
    # Iterate over the list of objects
    for object in objects['Contents']:
        # Get the object from the bucket
        obj = s3.get_object(Bucket=deafult_target_bucket, Key=object['Key'])

        # Read the object's content and convert it to a string
        file_content = obj['Body'].read().decode('utf-8')

        # Parse the log file content
        for line in file_content.splitlines():
            lines.append(line)

    return lines


def pull_existing_log_files():
    """
    {
        'Key': 'logs/2023-03-22-20-21-24-77D1758EEBE7057C',
        'LastModified': datetime.datetime(2023, 3, 22, 20, 21, 25, tzinfo=tzutc()),
        'ETag': '"4f21826cb131b05e4e23f4aa2583723a"',
        'Size': 2310,
        'StorageClass': 'STANDARD',
        'Owner': {'DisplayName': 'caiolauro','ID': 'e7118901f8198b997a28081b7cc5a2ed73f0ebe0747f4187bc20759203ce15e2'}
    }
    """
    # Iterate over the list of objects
    for object in objects['Contents']:
        file_key = object['Key'].split('/')[1].split('-')[-1]
        print(file_key)


if __name__ == '__main__':
    # logs = read_logs_from_s3()
    # write_full_logs(logs)
    pull_existing_log_files()
