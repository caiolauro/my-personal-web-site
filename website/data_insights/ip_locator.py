from requests import get
import json


# Ref: https://api.iplocation.net/]

def get_location_info_from_ip(ip_address): return get(
    f"https://api.iplocation.net/?ip={ip_address}").text


def transform_to_dict(text): return json.loads(text)
def get_country(json_data): return json_data['country_name']
